---
title: Curating, Annotating A Reading List of AWESOME materials
subtitle: Readings in the Science of Smell, Taste, Perceptions/Biases, Crowdliness/Policy
comments: false
---


## Smell and Odor

## Taste and Flavors

## Subjective Peceptions / Biases

## Crowdliness / Policy

### Measuring Subjective Sentiments/Opinions

We can expect that survey and market indicators will be especially error prone ... largely, because preferences [of increasing hysteria] are bound to be especially inconsistent, eg we can survey a group of people, asking the same [or a very similar, but rephrased] question ten times and get ten different answers, although we might expect some convergence [if the people in the group talk to others about their opinion of the subject matter in the survey question]  ... so we need to look at ways to understand sampling error like bootstrapping analysis and a variety of different [bayesian variance characterization techniques for the approximation of induced errors](https://www.connectedpapers.com/main/521e5c337be51b8f8fdb858580bb46a0545ab1f9/When-Gaussian-Process-Meets-Big-Data-A-Review-of-Scalable-GPs/graph) 