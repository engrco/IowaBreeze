---
title: About Iowa Breeze
subtitle: Why you SHOULD laugh about it.
comments: false
---

If we wanted produce perfumes or malodorous scented candles ... we need to UNDERSTAND what drives something to stink and to get on people's nerves. 

Any moron can just complain ... but it takes a bigger moron to get offended. 

It'd be better if we can all understand why something becomes an annoyance. 

Laughing about is the first step to understanding odor or taste and how humans sense it is the key to eliminating the problems ... unfortunately, there are thin-skinned people without any sense of humor who do not realize why their thin-skinnedness is the cause of why something that just is becomes a festering problem that nobody wants to address. 

Malodorous smells are a problem because people get their feelings hurt and stop laughing and communicating about fixing the problem.