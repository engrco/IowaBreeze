Start with [Book](bookURL) and then create a new notebook in [Google Colaboratory](https://colab.research.google.com/notebooks/intro.ipynb#scrollTo=OwuxHmxllTwN)

# WHY do you want to delve into this topic?  

# What did you learn, eg, disappointments, WOWs, aggravations ... that you did not expect to learn from this topic?